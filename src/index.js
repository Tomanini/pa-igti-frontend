import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Header from './common/header'
import Footer from './common/footer'
import about from './template/about';
import howitworks from './template/howitworks';
import contact from './template/contact';
import FAQ from './template/FAQ';
import privacy from './template/privacy';
import terms from './template/terms';
import admin from './template/admin/home'
import { Fragment } from 'react';

const routes = (
  <BrowserRouter>


      <Switch>
          {/* <BrowserRouter basename='/admin'> */}
            <Route path='/admin' component={admin} />
          {/* </BrowserRouter> */}
          <Fragment>
          <Header />
          <Route path="/" exact={true} component={App} />
          <Route path="/about" component={about} />
          <Route path="/howitworks" component={howitworks} />
          <Route path="/contact" component={contact} />
          <Route path="/faq" component={FAQ} />
          <Route path="/privacy" component={privacy} />
          <Route path="/terms" component={terms} />
          <Footer />
          </Fragment>
      </Switch>

    

    


  </ BrowserRouter>
)

ReactDOM.render(
routes, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
