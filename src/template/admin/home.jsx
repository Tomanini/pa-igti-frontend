import React from "react";
import axios from "axios";
import "../../config/API";
import { Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';
import { CategoryCreate, CategoryList, CategoryEdit } from './categories'
import { ItemEdit, ItemList } from './items'
import { CharitableFoundationsList } from './charitableFoundations'

export default class admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }




  render() {
    const dataProvider = jsonServerProvider('http://pa-igti-backend.herokuapp.com/api');
    // const dataProvider = jsonServerProvider('http://localhost:5000/api');
    return (
      <Admin dataProvider={dataProvider} >
        <Resource name="categories" options={{ label: 'Categorias' }}  list={CategoryList} create={CategoryCreate} edit={CategoryEdit} />
        <Resource name="items" options={{ label: 'Ítens' }} list={ItemList} edit={ItemEdit}/>
        <Resource name="charitableFoundations" options={{ label: 'Entidades' }} list={CharitableFoundationsList} />
      </Admin>
    );
  }
}

