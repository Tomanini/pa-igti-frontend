// in src/posts.js
import * as React from "react";
import { Create, Edit, EditButton, SimpleForm, TextInput, DateInput, ReferenceManyField, List, Datagrid, TextField, DateField, BooleanField } from 'react-admin';
// import { Datagrid, TextField, DateField, EditButton } from 'react-admin';

export const CharitableFoundationsList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="phone" />
            <TextField source="email" />
            <DateField source="createdAt" />
            <EditButton />
        </Datagrid>
    </List>
);

export const CharitableFoundationsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="description" />
            
        </SimpleForm>
    </Create>
);

export const CharitableFoundationsEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled label="Id" source="id" />
            <TextInput source="name" />
            <TextInput source="description" />
            <DateInput label="Created at" source="createdAt" />
            {/* <ReferenceManyField label="Items" reference="items" >
                <Datagrid>
                    <TextField source="name" />
                    <DateField source="created_at" />
                    <EditButton />
                </Datagrid>
            </ReferenceManyField> */}
        </SimpleForm>
    </Edit>
);