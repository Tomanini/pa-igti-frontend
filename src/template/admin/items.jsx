// in src/posts.js
import * as React from "react";
import { Create, Edit, EditButton, SimpleForm, TextInput, DateInput, ReferenceManyField, List, Datagrid, TextField, DateField, BooleanField } from 'react-admin';
// import { Datagrid, TextField, DateField, EditButton } from 'react-admin';

export const ItemList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="name" label="Nome" />
            <TextField source="description" label="Descrição"/>
            <DateField source="createdAt" label="Data de cadastro"/>
            <EditButton />
        </Datagrid>
    </List>
);

export const ItemCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="description" />
            
        </SimpleForm>
    </Create>
);

export const ItemEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled label="Id" source="id" />
            <TextInput source="name" />
            <TextInput source="description" />
            <DateInput label="Created at" source="createdAt" />

            {/* <ReferenceManyField label="Items" reference="items" >
                <Datagrid>
                    <TextField source="name" />
                    <DateField source="created_at" />
                    <EditButton />
                </Datagrid>
            </ReferenceManyField> */}
        </SimpleForm>
    </Edit>
);