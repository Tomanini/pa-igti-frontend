// in src/posts.js
import * as React from "react";
import { Toolbar, Create, Edit, EditButton, SimpleForm, TextInput, DateInput, ReferenceManyField, List, Datagrid, TextField, DateField, BooleanField, SaveButton } from 'react-admin';
// import { Datagrid, TextField, DateField, EditButton } from 'react-admin';

export const CategoryList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <DateField source="createdAt" />
            <TextField source="description" />
            <EditButton />
        </Datagrid>
    </List>
);

export const CategoryCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="description" />
            
        </SimpleForm>
    </Create>
);

export const CategoryEdit = (props) => (
    <Edit undoable={false} {...props}>
        <SimpleForm toolbar={<PostEditToolbar />}>
            <TextInput disabled label="Id" source="id" />
            <TextInput source="name" />
            <TextInput source="description" />
            <DateInput label="Created at" source="createdAt" />
            {/* <ReferenceManyField label="Items" reference="items" >
                <Datagrid>
                    <TextField source="name" />
                    <DateField source="created_at" />
                    <EditButton />
                </Datagrid>
            </ReferenceManyField> */}
            {/* <SaveButton
            redirect="show"
            submitOnEnter={true}
            /> */}
        </SimpleForm>
    </Edit>
);

const PostEditToolbar = props => (
    <Toolbar {...props} >
        {/* <SaveButton disabled={!props.pristine} /> */}
    </Toolbar>
);