import React from "react";
import axios from "axios";
import "../config/API";
import Search from "./search";
import EntityCard from './../common/entityCard'
import './../common/styles.css'

export default class home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      entities: [],
      searchValue: ''
    };
    this.handleSearch = this.handleSearch.bind(this);
  }

  searchResultsLabel(){
    if(this.state.searchValue !== ''){
      return <blockquote>Instituições que precisam de <i>{this.state.searchValue}</i></blockquote>
    }else{
      return false
    }
  }

  async handleSearch(){
      let search =  document.getElementById('search').value
      await axios.get(`/search?search=${search}`)
      .then(res => {
        const entities = res.data;
        if(entities){
          this.setState({ entities, loading: false, searchValue: search });
          this.searchResultsLabel()
        }else{
          this.setState({ loading: false })
        }
      })
  }

  render() {
    return (
        <div className="container main-container">
              <Search searchFunction={this.handleSearch} />
            
        {this.searchResultsLabel()}
        <div class="row">
        { this.state.entities.map(entity => (
          <EntityCard entity={entity}/>
        ))}
        </div>
        </div>
    );
  }
}

