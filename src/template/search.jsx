import React from "react";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      items: [],
      inputValue: '',

    };
  }

  async componentDidMount() {
    let items = [];
    this.setLoadingStatus(true)
    await axios.get(`/items`).then((res) => {
      items = res.data;
      this.setState({ items: res.data, loading: false });
    });
  }

  setInputValue(value) {
    this.setState({ inputValue: value })
  }

  setLoadingStatus(status) {
    this.setState({ loading: status })
  }



  render() {
    const items = this.state.items
    const loading = this.state.loading

    if (loading) {
      return (
        <div class="progress">
          <div class="indeterminate"></div>
        </div>
      )
    } else {
      return (
        <React.Fragment>
          <Autocomplete
            autoHighlight
            id="search"
            name="search"
            options={items}
            getOptionLabel={(option) => (option.name)}
            inputValue={this.state.inputValue}
            onInputChange={async (event, newInputValue) => {

              event.preventDefault()
              await this.setInputValue(newInputValue);
              await this.props.searchFunction()

            }}
            renderInput={(params) => (
              <TextField {...params} label="O que você quer doar?" v />
            )}
            onClose={(event) => {
              event.preventDefault()
            }}
            onSelect={() => {

            }}
            onOpen={() => {

            }}

          />
        </React.Fragment>
      );
    }
  }
}
