import React from "react";

export default (props) => {

  return (
    <footer class="page-footer grey">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">iDonate</h5>
            <p class="grey-text text-lighten-4">Plataforma de doações. Projeto aplicado do curso de desenvolvimento fullstack IGTI.</p>
          </div>
          <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="#!">Entidades</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Blog</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Como funciona</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Contato</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
          © 2020 Copyright Text
      <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
      </div>
    </footer>
  );
};
