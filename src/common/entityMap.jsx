import React from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const EntityMap = withScriptjs(withGoogleMap((props) =>
<div>
    
<GoogleMap
  defaultZoom={15}
  defaultCenter={{ lat: -30.0281574, lng: -51.2308308 }}
><h1>{props.long}</h1>
  {props.isMarkerShown && <Marker position={{ lat: -30.0281574, lng: -51.2308308 }} />}
  
</GoogleMap>

</div>
))

export default EntityMap

// {/* <MyMapComponent isMarkerShown />// Map with a Marker */}
// {/* <MyMapComponent isMarkerShown={false} />// Just only Map */}