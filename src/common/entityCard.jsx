import React from "react";
import NavigationIcon from "@material-ui/icons/Navigation";
import axios from 'axios'
import "../config/API";
import modal from 'materialize-css/js/modal'
import "materialize-css/dist/css/materialize.min.css";
import EntityMap from './entityMap'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

// export default function EntityCard(props) {
export default class EntityCard extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      entity: [],
    };
  }

  async componentDidMount(){
    // alert(this.props.entity.id)
    await axios.get(`/charitableFoundations/${this.props.entity.id}`)
      .then(res => {
        const entity = res.data;
        
        if(entity){
          this.setState({ entity: entity, loading: false });
          // this.searchResultsLabel()
        }else{
          this.setState({ loading: false })
        }
      })




  }


render(){
  const entity = this.state.entity
  return (
    <div class="col s12 m6" key={entity.id}>
    <div class="card blue-grey darken-1">
      <div class="card-content white-text">
        <span class="card-title">{entity.name}</span>
        <p>{entity.email}</p>
        <h6>Onde entregar doações: </h6>
        <p> {entity.address_street + " " + entity.address_number}
           <p>CEP: {entity.address_zipcode}</p>
        </p> 
      </div>
      <h6>A instituição também precisa de:</h6>
      <p>
      {
      entity.items ?
        entity.items.map(item => <div class="chip">{item.name}</div>)
      :
      ''
      }
      </p>

      <div class="card-action">
        {
        entity.latitude !== '' ?
        
        <p>
          {/* {entity.latitude} */}
          <EntityMap   
              isMarkerShown
              googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
              loadingElement={<div style={{ height: `100%` }} />}
              containerElement={<div style={{ height: `400px` }} />}
              mapElement={<div style={{ height: `100%` }} 
              lat={entity.latitude}
              long='aaaaa'
              />}
        />

        </p>
        :
        ''
        }       
      </div>
    </div>
  </div>
  );
}

}
