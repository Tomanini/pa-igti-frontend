import React from "react";

export default (props) => {
  return (
    <React.Fragment>
      <nav>
        <div class="nav-wrapper green">
          <a href="" class="brand-logo center">
            iDonate
          </a>
          <ul id="nav-mobile" class="right hide-on-med-and-down ">
            <li>
              <a href="">Como funciona</a>
            </li>
            <li>
              <a href="">Quem somos</a>
            </li>
            <li>
              <a href="">FAQ</a>
            </li>
          </ul>
        </div>
      </nav>
    </React.Fragment>
  );
};
