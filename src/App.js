import React from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './template/home';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

function App() {

  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = createMuiTheme({
    // status: {
      // danger: '#e53e3e',
    // },
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    // palette: {
    //   neutral: {
    //     main: '#5c6ac4',
    //   },
    // },
  });

  // const theme = {
  //   background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
  // };

  return (
    <div className="App">
        {/* <ThemeProvider theme={theme}>
      <CssBaseline/> */}
      <body>
        <main>
      <Home />
      </main>
      </body>
    {/* </ThemeProvider> */}
    </div>
  );
}

export default App;
